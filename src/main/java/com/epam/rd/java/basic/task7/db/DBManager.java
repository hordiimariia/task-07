package com.epam.rd.java.basic.task7.db;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.*;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;

import com.epam.rd.java.basic.task7.db.entity.*;

public class DBManager {
	Properties properties = new Properties();
	private static DBManager instance;
	final String url;

	public static synchronized DBManager getInstance() {
		if (instance == null) {
			instance = new DBManager();
		}
		return instance;
	}

	private DBManager() {
		try {
			Class.forName("com.mysql.cj.jdbc.Driver");
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		try {
			FileInputStream fileInputStream = new FileInputStream("app.properties");
			properties.load (fileInputStream);
		} catch (IOException e) {
			e.printStackTrace();
		}
		//url = "jdbc:mysql://localhost:3306/test2db";
		url = properties.getProperty("connection.url");
	}

	public List<User> findAllUsers() throws DBException {
		List<User> allUsers = new ArrayList<>();

		try (Connection con = DriverManager.getConnection(url);
			Statement statement = con.createStatement())
		{
			String sql = "SELECT id, login FROM users";
			ResultSet resSet = statement.executeQuery(sql);

			while (resSet.next()){
				int id = resSet.getInt(1);
				String login = resSet.getString(2);
				allUsers.add(new User(id, login));
			}
		}
		catch (SQLException e) {
			e.printStackTrace();
		}
		return allUsers;
	}

	public boolean insertUser(User user) throws DBException {
		int res = 0;
		try (Connection connection = DriverManager.getConnection(url))
		{
			String sql = "INSERT INTO users(login) VALUES  (?)";
			PreparedStatement statement = connection.prepareStatement(sql);
			List<User> users = DBManager.getInstance().findAllUsers();
			if(!users.contains(user)) {
//				statement.setInt(1, user.getId());
				statement.setString(1, user.getLogin());
				res = statement.executeUpdate();
				User temp = new DBManager().getUser(user.getLogin());
				user.setId(temp.getId());
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return res != 0;
	}

	public boolean deleteUsers(User... users) throws DBException {
		List<User> userList = new ArrayList<>(List.of(users));
		Iterator<User> iterator = userList.iterator();
		Connection connection = null;
		PreparedStatement statement = null;
		int res = 0;
		try {
			connection = DriverManager.getConnection(url);
			connection.setAutoCommit(false);
			while (iterator.hasNext()){
				String sql = "DELETE FROM users WHERE users.id = ?";
				statement = connection.prepareStatement(sql);
				statement.setInt(1, iterator.next().getId());
				res += statement.executeUpdate();
			}
		} catch (SQLException e) {
			try{
				if(connection != null){
					connection.rollback();
				}
			}
			catch (SQLException ex){
				ex.printStackTrace();
			}
		}
		finally {
			try{
				if (connection != null) {
					connection.setAutoCommit(true);
				}
			}
			catch (SQLException e){
				e.printStackTrace();
			}
			try {
				if (connection != null) {
					connection.close();
				}
				if (statement != null) {
					statement.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}

		return res != 0;
	}

	public User getUser(String login) throws DBException {
		User user = null;
		String sql = "SELECT id, login FROM users WHERE login=(?)";
		try (Connection con = DriverManager.getConnection(url);
			 PreparedStatement statement = con.prepareStatement(sql))
		{
			statement.setString(1, login);

			ResultSet resSet = statement.executeQuery();
			if (resSet.next()){
				user = new User(resSet.getInt("id"), resSet.getString("login"));
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}
		if(user != null && user.getLogin().equals(login)){
			return user;
		}
		else {
			return null;
		}
	}

	public Team getTeam(String name) throws DBException {
		Team team = null;
		String sql = "SELECT id, name FROM teams WHERE name=(?)";

		try (Connection con = DriverManager.getConnection(url);
			 PreparedStatement statement = con.prepareStatement(sql))
		{
			statement.setString(1, name);

			ResultSet resSet = statement.executeQuery();
			if (resSet.next()){
				team = new Team(resSet.getInt("id"), resSet.getString("name"));
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}
		if(team != null && team.getName().equals(name)){
			return team;
		}
		else {
			return null;
		}
	}

	public List<Team> findAllTeams() throws DBException {
		List<Team> allTeams = new ArrayList<>();

		try (Connection allTeamsConnection = DriverManager.getConnection(url);
			Statement allTeamsStatement = allTeamsConnection.createStatement())
		{
			String sqlAllTeams = "SELECT id, name FROM teams";
			ResultSet resultSetAllTeams = allTeamsStatement.executeQuery(sqlAllTeams);

			while (resultSetAllTeams.next()){
				int id = resultSetAllTeams.getInt(1);
				String name = resultSetAllTeams.getString(2);
				allTeams.add(new Team(id, name));
			}
		}
		catch (SQLException e) {
			e.printStackTrace();
		}
		return allTeams;
	}

	public boolean insertTeam(Team team) throws DBException {
		int res = 0;
		PreparedStatement statement = null;
		try (Connection con = DriverManager.getConnection(url))
		{
			List<Team> teams = DBManager.getInstance().findAllTeams();
			if(!teams.contains(team)) {
				String sql = "INSERT INTO teams(name) VALUES (?)";
				statement = con.prepareStatement(sql);
//				statement.setInt(1, team.getId());
				statement.setString(1, team.getName());
				res = statement.executeUpdate();
				Team temp = new DBManager().getTeam(team.getName());
				team.setId(temp.getId());
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		finally {
			try {
				if (statement != null) {
					statement.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return res != 0;
	}

	public boolean setTeamsForUser(User user, Team... teams) throws DBException {
		List<Team> teamList = new ArrayList<>(List.of(teams));
		Iterator<Team> iterator = teamList.iterator();
		Connection con = null;
		PreparedStatement statement = null;
		int res = 0;
		try {
			con = DriverManager.getConnection(url);
			con.setAutoCommit(false);
			while (iterator.hasNext()){
				String sql = "INSERT INTO users_teams(user_id, team_id) VALUES (?, ?)";
				statement = con.prepareStatement(sql);
				statement.setInt(1, user.getId());
				statement.setInt(2, iterator.next().getId());
				res += statement.executeUpdate();
			}

		} catch (SQLException e) {
			try{
				if(con != null){
					con.rollback();
					throw new DBException("null", e);
				}
			}
			catch (SQLException ex){
				ex.printStackTrace();
			}
		}
		finally {
			try{
				if (con != null) {
					con.setAutoCommit(true);
				}
			}
			catch (SQLException e){
				e.printStackTrace();
			}
			try {
				if (con != null) {
					con.close();
				}
				if (statement != null) {
					statement.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return res != 0;
	}

	public List<Team> getUserTeams(User user) throws DBException {
		List<Team> teams = new ArrayList<>();
		String sqlID = "SELECT teams.id, teams.name " +
				"FROM users_teams " +
				"INNER JOIN users ON users_teams.user_id = users.id " +
				"INNER JOIN teams ON users_teams.team_id = teams.id " +
				"WHERE user_id = ?";
		try (Connection connection = DriverManager.getConnection(url);
			 PreparedStatement statement = connection.prepareStatement(sqlID))
		{
			statement.setInt(1, user.getId());
			ResultSet rs = statement.executeQuery();
			while (rs.next()){
				int id = rs.getInt(1);
				String name = rs.getString(2);
				teams.add(new Team(id, name));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return teams;
	}

	public boolean deleteTeam(Team team) throws DBException {
		int res = 0;
		String sql = "DELETE FROM teams WHERE teams.id = ?";
		try (Connection connection = DriverManager.getConnection(url);
			 PreparedStatement statement = connection.prepareStatement(sql))
		{
			statement.setInt(1, team.getId());
			res = statement.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return res != 0;
	}

	public boolean updateTeam(Team team) throws DBException {
		int res = 0;
		String sql = "UPDATE teams " +
				"SET name = ? " +
				"WHERE id = ?";
		try (Connection connection = DriverManager.getConnection(url);
			 PreparedStatement statement = connection.prepareStatement(sql))
		{
			statement.setString(1, team.getName());
			statement.setInt(2, team.getId());
			res = statement.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return res != 0;
	}

}
